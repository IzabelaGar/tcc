﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tcc.Models
{

    public enum TipoPorte
    {
        [Display(Name = "Não Sabe")]
        Nenhum = 0,
        [Display(Name = "Mini")]
        Mini,
        [Display(Name = "Pequeno")]
        Pequeno,
        [Display(Name = "Médio")]
        Medio,
        [Display(Name = "Grande")]
        Grande,
        [Display(Name = "Super grande")]
        SuperGrande

    }
}
