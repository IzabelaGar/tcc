using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using Tcc.Models;

namespace Tcc.ViewModels
{
    public class PerfilViewModel
    {
        public PerfilViewModel()
        {

        }

        public PerfilViewModel(Usuario esteUsuario)
        {

            Nome = esteUsuario.Nome;
        }

        [Display(Name = "Nome Completo")]
        [Required]
        [MaxLength]
        public string Nome { get; set; }

    }
}
