using System.ComponentModel.DataAnnotations;


namespace Tcc.ViewModels
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
