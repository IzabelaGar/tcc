﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tcc.Models
{
    public class Raca
    {
        public int RacaId { get; set; }
        [MaxLength(200, ErrorMessage = "O limite do campo é {0}")]
        [Display(Name = "Raça")]
        public string NomeRaca { get; set; }
        public virtual ICollection<Animal> Animais { get; set; }
    }
}
