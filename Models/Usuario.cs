﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tcc.Models
{
    public class UsuarioMetadata
    {
        [Display(Name = "Login")]
        public string UserName { get; set; }
    }

    public class Usuario : IdentityUser<Guid>
    {
        public string Nome { get; set; }
        public DateTime Nasci { get; set; }
        public virtual ICollection<Animal> Animais { get; set; }


    }
}
