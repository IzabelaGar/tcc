﻿#region Using

using System.ComponentModel.DataAnnotations;

#endregion

namespace Tcc.ViewModels
{
    public class LoginWithRecoveryCodeViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Recovery Code")]
        public string RecoveryCode { get; set; }
    }
}
