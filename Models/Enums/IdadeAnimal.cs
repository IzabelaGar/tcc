﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tcc.Models.Enums
{
    public enum IdadeAnimal
    {
        [Display(Name = "Não Sabe")]
        NaoSabe = 0,
        [Display(Name = "Filhote")]
        Filhote,
        [Display(Name = "Adulto")]
        Adulto,
        [Display(Name = "Idoso")]
        Idoso
    }
}
