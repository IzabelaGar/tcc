﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Tcc.Migrations
{
    public partial class DB_mudanca1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Animal_Porte_PorteId",
                table: "Animal");

            migrationBuilder.DropTable(
                name: "Porte");

            migrationBuilder.DropIndex(
                name: "IX_Animal_PorteId",
                table: "Animal");

            migrationBuilder.DropColumn(
                name: "PorteId",
                table: "Animal");

            migrationBuilder.AddColumn<int>(
                name: "Idade",
                table: "Animal",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Porte",
                table: "Animal",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Idade",
                table: "Animal");

            migrationBuilder.DropColumn(
                name: "Porte",
                table: "Animal");

            migrationBuilder.AddColumn<int>(
                name: "PorteId",
                table: "Animal",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Porte",
                columns: table => new
                {
                    PorteId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PorteTamanho = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Porte", x => x.PorteId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Animal_PorteId",
                table: "Animal",
                column: "PorteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Animal_Porte_PorteId",
                table: "Animal",
                column: "PorteId",
                principalTable: "Porte",
                principalColumn: "PorteId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
