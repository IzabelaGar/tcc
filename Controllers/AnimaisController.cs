﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Tcc.Data;
using Tcc.Extensions;
using Tcc.Models;

namespace Tcc.Controllers
{
    [Authorize]
    public class AnimaisController : Controller
    {
        private readonly TccContext _context;

        public AnimaisController(TccContext context)
        {
            _context = context;
        }

        // GET: Animais
        public async Task<IActionResult> Index()
        {
            var tccContext = _context.Animal.Include(a => a.Racas).Include(a => a.Usuarios);
            return View(await tccContext.ToListAsync());
        }

        // GET: Animais/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var animal = await _context.Animal
                .Include(a => a.Racas)
                .Include(a => a.Usuarios)
                .FirstOrDefaultAsync(m => m.AnimalId == id);
            if (animal == null)
            {
                return NotFound();
            }

            return View(animal);
        }

        // GET: Animais/Create
        public IActionResult Create()
        {
            var animal = new Animal();

            ViewData["RacaId"] = new SelectList(_context.Raca.ToList(), "RacaId", "NomeRaca");
            ViewData["Porte"] = this.MontarSelectListParaEnum(animal.Porte);
            ViewData["Idade"] = this.MontarSelectListParaEnum(animal.Idade);
            ViewData["UsuariosId"] = new SelectList(_context.Users, "Id", "Id");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Animal animal)
        {
            if (ModelState.IsValid)
            {
                //var UserGuid = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
                //animal.UsuariosId = UserGuid;

                animal.AnimalId = Guid.NewGuid();
                _context.Add(animal);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["RacaId"] = new SelectList(_context.Set<Raca>(), "RacaId", "RacaId", animal.RacaId);
            ViewData["UsuariosId"] = new SelectList(_context.Users, "Id", "Id", animal.UsuariosId);
            return View(animal);
        }

        // GET: Animais/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var animal = await _context.Animal.FindAsync(id);
            if (animal == null)
            {
                return NotFound();
            }
            ViewData["RacaId"] = new SelectList(_context.Set<Raca>(), "RacaId", "RacaId", animal.RacaId);
            ViewData["UsuariosId"] = new SelectList(_context.Users, "Id", "Id", animal.UsuariosId);
            return View(animal);
        }

        // POST: Animais/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("AnimalId,Nome,Cor,RacaId,Porte,Idade,UsuariosId")] Animal animal)
        {
            if (id != animal.AnimalId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(animal);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AnimalExists(animal.AnimalId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["RacaId"] = new SelectList(_context.Set<Raca>(), "RacaId", "RacaId", animal.RacaId);
            ViewData["UsuariosId"] = new SelectList(_context.Users, "Id", "Id", animal.UsuariosId);
            return View(animal);
        }

        // GET: Animais/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var animal = await _context.Animal
                .Include(a => a.Racas)
                .Include(a => a.Usuarios)
                .FirstOrDefaultAsync(m => m.AnimalId == id);
            if (animal == null)
            {
                return NotFound();
            }

            return View(animal);
        }

        // POST: Animais/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var animal = await _context.Animal.FindAsync(id);
            _context.Animal.Remove(animal);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AnimalExists(Guid id)
        {
            return _context.Animal.Any(e => e.AnimalId == id);
        }
    }
}
