﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Tcc.Models;

namespace Tcc.Data
{
    public class TccContext : IdentityDbContext<Usuario, Funcao, Guid>
    {

        public TccContext(DbContextOptions<TccContext> options) : base(options)
        {

        }

        public DbSet<Tcc.Models.Animal> Animal { get; set; }

        public DbSet<Tcc.Models.Raca> Raca { get; set; }



        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    base.OnConfiguring(optionsBuilder);

        //    optionsBuilder.EnableSensitiveDataLogging();
        //}

    }
}
